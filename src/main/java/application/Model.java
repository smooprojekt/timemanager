package application;

import java.util.ArrayList;

public class Model {
    @SuppressWarnings("unused")
    private View view;
    private Project selectedProject;
    private ArrayList<Project> projects;
    public boolean loggedIn;
    private User loggedInUser;
    private int userCount;

    private ArrayList<User> users;

    //Martin
    public Model()
    {
        this.userCount = 0;
        this.projects = new ArrayList<Project>();
        this.users = new ArrayList<User>();
        this.loggedIn = false;
        this.loggedInUser = null;
    }

    //Martin
    public void createProject(String projectName, long projectStartDate, long projectEndDate)
    {
        if(loggedInUser.getClearance() == 1)
        {
            projects.add(new Project(projectName, projectStartDate, projectEndDate));
        }
    }

    //Martin
    public void finishProject(Project project)
    {
        project.setFinished(true);
    }

    //Martin
    public Project getSelectedProject()
    {
        return selectedProject;
    }

    //Martin
    public void setSelectedProject(Project selectedProject)
    {
        this.selectedProject = selectedProject;
    }

    public ArrayList<Project> getProjects()
    {
        return projects;
    }

    //Martin
    public int checkClearance(User user)
    {
        return user.getClearance();
    }

    //Martin
    public void createTask(Project project, String taskName, long taskStartDate, long taskEndDate, String taskDescription, int userCount)
    {
        project.createTask(taskName, taskStartDate, taskEndDate, taskDescription, userCount);
    }

    //Martin
    public void finishTask(Project project, Task task)
    {
        project.finishTask(task);
    }

    //Martin
    public boolean hasProjectWithName(String name)
    {
        for(int i = 0; i < projects.size(); i++)
        {
            if(projects.get(i).getName().equals(name))
            {
                return true;
            }
        }

        return false;
    }

    //Martin
    public Project projectFromName(String name)
    {
        for(int i = 0; i < projects.size(); i++)
        {
            if(projects.get(i).getName().equals(name))
            {
                return projects.get(i);
            }
        }

        return null;
    }

    //Martin
    public void logIn(User user)
    {
        this.loggedIn = true;
        this.loggedInUser = user;

        if(!users.contains(user))
        {
            //assign new user
            
            user.setID(userCount);

            userCount++;

            users.add(user);

            //update tasks
            for(Project project : projects)
            {
                for(Task task : project.getTasks())
                {
                    task.getTimeList().add(0.0d);
                }
            }
        }
    }

    //Martin
    public int getUserCount()
    {
        return userCount;
    }

    public User getUser()
    {
        return loggedInUser;
    }
}
