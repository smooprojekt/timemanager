package application;

import java.util.ArrayList;

public class User {
    private int clearance;
    private String name;
    private int id;

    //Martin
    public User(String name, int clearance)
    {
        this.name = name;
        this.clearance = clearance;
    }

    //Martin
    public void assignTime(Task task, double hours)
    {
        task.getTimeList().set(this.id,task.getTimeList().get(this.id) + hours);
    }

    public double getTimeTotal(Project project){ //sigurd
        double hours = 0;
        ArrayList<Task> tasks = new ArrayList<Task>();
        tasks = project.getTasks();
        int id = this.id;
        for(int i = 0; i<tasks.size(); i++){
            ArrayList<Double> timelist = new ArrayList<Double>();
            timelist = tasks.get(i).getTimeList();
            hours += timelist.get(id);
        }
        return hours;
    }

    //Martin
    public String getName()
    {
        return name;
    }

    //Martin
    public int getClearance()
    {
        return clearance;
    }

    //Martin
    public void setClearance(int clearance)
    {
        this.clearance = clearance;
    }

    //Martin
    public void setID(int id)
    {
        this.id = id;
    }

    //Martin
    public int getID()
    {
        return id;
    }
}
