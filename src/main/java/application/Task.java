package application;

import java.util.ArrayList;

public class Task {
    private String name;
    private long unixStartDate;
    private long unixEndDate;
    private boolean finished;
    private String description;
    private ArrayList<Double> timeList;

    public CheckList checkList = new CheckList();

    public long x;
    public int y;
    public double width;

    //Martin
    public Task(String name, long startDate, long endDate, String description, int userCount)
    {
        this.name = name;
        this.unixStartDate = startDate;
        this.unixEndDate = endDate;
        this.description = description;
        this.timeList = new ArrayList<Double>();
        for(int i = 0; i < userCount; i++)
        {
            this.timeList.add(0.0);
        }
    }

    //Martin
    public boolean isFinished()
    {
        return this.finished;
    }

    //Martin
    public ArrayList<Double> getTimeList()
    {
        return timeList;
    }

    //Martin
    public double getTotalTime()
    {
        double total = 0;
        
        for(int i = 0; i < this.timeList.size(); i++)
        {
            total += this.timeList.get(i);
        }
        
        return total;
    }

    //Martin
    public void setFinished(boolean finished)
    {
        this.finished = finished;
    }

    //Martin
    public void setDescription(String description)
    {
        this.description = description;
    }

    //Martin
    public String getDescription()
    {
        return this.description;
    }

    //Martin
    public String getName()
    {
        return this.name;
    }

    //Martin
    public void setName(String name)
    {
        this.name = name;
    }

    //Martin
    public long getStartDate()
    {
        return this.unixStartDate;
    }

    //Martin
    public long getEndDate()
    {
        return this.unixEndDate;
    }
}
