package application;


import java.util.ArrayList;
import javafx.scene.paint.Color;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import java.time.Instant;

public class View extends Application {

    public Model model;
    private Pane root = new Pane();

    private long UNIX_START = 1672531200;
    private long UNIX_FIRST = ((getCurrentTime() - UNIX_START) - 1) - ((getCurrentTime() - UNIX_START) - 1) % 60;

    private int taskH = 80/2;
    private int taskW = 60;
    private Task hoverTask = null;
    private Project hoverProject = null;
    private String taskResize = "nothing";

    private long hoverX = 0;
    private int hoverY = 0;
    private int hoverTaskW = 0;
    private long hoverTaskX = 0;
    @SuppressWarnings("unused")
    private int hoverTaskY = 0;
    private int mouseTaskX = 0;
    private int mouseTaskY = 0;
    @SuppressWarnings("unused")
    private int mouseTaskW = taskW;
    private Task selectedTask = null;

    private boolean controlGetKey;

    private int canvasWidth = 640;
    private int canvasHeight = 470;
    private long scrollX = -UNIX_FIRST/60/24 + canvasWidth/2;

    private boolean editMenu;
    private Rectangle editRectangle = null;
    private Rectangle hoursRectangle = null;

    TextArea editNameTextArea;
    TextArea editDescriptionTextArea;
    TextArea hoursTextArea;
    TextField projectName = null;
    CheckBox projectFinished;

    Text taskHoursDisplay;

    CheckBox taskComplete = new CheckBox("TaskComplete");

    ArrayList<CheckBox> CheckBoxList = new ArrayList<>();
    ArrayList<TextArea> textAreaList = new ArrayList<>();

    //Oskar
    public void drawCheckList(Pane root, Double x, Double y, Double yoffset, Task task){
        for(int i = 0; i < task.checkList.getListSize(); i += 1){
            CheckBox checkBox = new CheckBox("");
            
            checkBox.setSelected(task.checkList.getCheckListChecked(i));
            CheckBoxList.add(checkBox);
            checkBox.setLayoutX(x);
            checkBox.setLayoutY(y + yoffset * i);
            TextArea checkBoxTextArea = new TextArea();
            textAreaList.add(checkBoxTextArea);
            checkBoxTextArea.setText(task.checkList.getCheckListName(i));
            checkBoxTextArea.setMaxHeight(45);
            checkBoxTextArea.setMaxWidth(160);
            checkBoxTextArea.setWrapText(true);
            checkBoxTextArea.setLayoutX(x+20);
            checkBoxTextArea.setLayoutY(y-13 + yoffset * i);
            checkBoxTextArea.setStyle("-fx-control-inner-background: #1f1f1f;");

            root.getChildren().addAll(checkBox,checkBoxTextArea);
        }
    }

    //Oskar
    public void drawEditUI(){
        int rectX = 200;
        int rectY = 250;
        editRectangle = new Rectangle((canvasWidth - rectX) / 2, (canvasHeight - rectY) / 2, rectX, rectY);
        editNameTextArea = new TextArea();
        editNameTextArea.setLayoutX(editRectangle.getX()+5);
        editNameTextArea.setLayoutY(editRectangle.getY()+5);
        editNameTextArea.setPrefWidth(editRectangle.getWidth() - 10);
        editNameTextArea.setMaxHeight(0);
        editNameTextArea.setText(selectedTask.getName());
        editNameTextArea.setFont(new Font(14));
        editNameTextArea.setWrapText(false);
        editNameTextArea.setMaxWidth(editRectangle.getWidth());
        editNameTextArea.setStyle("-fx-control-inner-background: #383838;");

        editDescriptionTextArea = new TextArea();
        editDescriptionTextArea.setLayoutX(editRectangle.getX()+5);
        editDescriptionTextArea.setLayoutY(editRectangle.getY()+5+48);
        editDescriptionTextArea.setFont(new Font(10));
        editDescriptionTextArea.setText(selectedTask.getDescription());
        editDescriptionTextArea.setPrefWidth(editRectangle.getWidth() - 10);
        editDescriptionTextArea.setWrapText(true);
        editDescriptionTextArea.setMaxHeight(140);
        editDescriptionTextArea.setStyle("-fx-control-inner-background: #1f1f1f;");

        taskComplete.setSelected(selectedTask.isFinished());
        taskComplete.setLayoutX(editRectangle.getX() + 5);
        taskComplete.setLayoutY(editRectangle.getY() + editRectangle.getHeight() - 23);
        taskComplete.setStyle("-fx-text-fill: white;");

        hoursRectangle = new Rectangle((canvasWidth/2 - rectX) / 2, (canvasHeight - rectY)*2/3, rectX/2+30, rectY/2);
        hoursTextArea = new TextArea();
        hoursTextArea.setLayoutX(hoursRectangle.getX()+5);
        hoursTextArea.setLayoutY(hoursRectangle.getY()+5);
        hoursTextArea.setPrefWidth(hoursRectangle.getWidth() - 10);
        hoursTextArea.setMaxHeight(0);
        hoursTextArea.setText("");
        hoursTextArea.setFont(new Font(14));
        hoursTextArea.setWrapText(false);
        hoursTextArea.setMaxWidth(hoursRectangle.getWidth());
        hoursTextArea.setStyle("-fx-control-inner-background: #383838;");

        Button addHours = new Button("Add Hours");
        addHours.setLayoutX(hoursRectangle.getX()+ hoursRectangle.getWidth()/2-36);
        addHours.setLayoutY(hoursRectangle.getY() + hoursRectangle.getHeight()-36);

        taskHoursDisplay = new Text();
        taskHoursDisplay.setLayoutX(hoursRectangle.getX()+hoursRectangle.getWidth()/2-36);
        taskHoursDisplay.setLayoutY(hoursRectangle.getY()+hoursRectangle.getHeight()/2);
        taskHoursDisplay.fillProperty().set(Color.WHITE);
        taskHoursDisplay.setText("Hours: " + selectedTask.getTotalTime());

        root.getChildren().addAll(editRectangle, editNameTextArea, editDescriptionTextArea, taskComplete);
        root.getChildren().addAll(hoursRectangle, hoursTextArea,addHours,taskHoursDisplay);
        
        editRectangle = new Rectangle(editRectangle.getX()+editRectangle.getWidth()+10,0,200,55*(selectedTask.checkList.getListSize()+1));
        editRectangle.setY(canvasHeight / 2 - editRectangle.getHeight()/2);
        root.getChildren().addAll(editRectangle);

        int heightDiff = (int)(editRectangle.getHeight()/(selectedTask.checkList.getListSize()+1));
        drawCheckList(root, editRectangle.getX() + 5, editRectangle.getY() + 18, (double)heightDiff,selectedTask);

        Button addCheckList = new Button("Add");
        addCheckList.setLayoutX(editRectangle.getX()+ editRectangle.getWidth()/2-18);
        addCheckList.setLayoutY(editRectangle.getY() + editRectangle.getHeight() - heightDiff / 1.4);
        root.getChildren().addAll(addCheckList);

        addCheckList.setOnAction(event -> {
            selectedTask.checkList.addListChecked(false);
            selectedTask.checkList.addListText("To Do");

            CheckBox checkBox = new CheckBox("");
            TextArea checkBoxTextArea = new TextArea(selectedTask.checkList.getCheckListName(selectedTask.checkList.getListSize()-1));
            CheckBoxList.add(checkBox);
            textAreaList.add(checkBoxTextArea);

            exitEditMenu(0,0);
            drawEditUI();
        });

        addHours.setOnAction(event ->{
            model.getUser().assignTime(selectedTask,getIntegerFromText(hoursTextArea));

            exitEditMenu(0,0);
            drawEditUI();
        });
    }

    //Oskar
    public void exitEditMenu(long mouseX, int mouseY){
        if (editRectangle != null){
            if (!(mouseX >= editRectangle.getX() && mouseY >= editRectangle.getY() 
            && mouseX <= editRectangle.getX() + editRectangle.getWidth() && mouseY <= editRectangle.getY() + editRectangle.getHeight())){
                editRectangle = null;
                editMenu = false;
                selectedTask.setName(editNameTextArea.getText());
                selectedTask.setDescription(editDescriptionTextArea.getText());
                for(int i = 0; i < selectedTask.checkList.getListSize(); i += 1){
                    selectedTask.checkList.setListChecked(i, CheckBoxList.get(i).isSelected());
                    selectedTask.checkList.setListText(i, textAreaList.get(i).getText());
                }

                CheckBoxList.clear();
                textAreaList.clear();
                update();
            } 
            selectedTask.setFinished(taskComplete.isSelected());
        }
    }

    //Martin
    private void drawGrid(Pane root, double timePosition){
        int interval = 1;

        if(taskW == 15)
        {
            interval = 4;
        }
        else if(taskW == 30)
        {
            interval = 2;
        }
        
        //small lines
        for(double i = timePosition - (timePosition % 60)*taskW/60 + scrollX; i >= 0; i -= taskW){
            Line gridLine = new Line(i,taskH*1.5,i,canvasHeight);
            gridLine.setStrokeWidth(1.5);
            gridLine.setStroke(Color.LIGHTGRAY);
            root.getChildren().add(gridLine);
        }

        //small lines
        for(double i = timePosition - (timePosition % 60)*taskW/60 + scrollX; i <= canvasWidth; i += taskW){
            Line gridLine = new Line(i,taskH*1.5,i,canvasHeight);
            gridLine.setStrokeWidth(1.5);
            gridLine.setStroke(Color.LIGHTGRAY);
            root.getChildren().add(gridLine);
        }
        
        int iterator = 0;

        //thick lines
        for(double i = timePosition - (timePosition % 60)*taskW/60 + scrollX; i >= 0; i -= taskW*interval){
            Line gridLine = new Line(i,taskH*1.5,i,canvasHeight);
            gridLine.setStrokeWidth(3);
            gridLine.setStroke(Color.GRAY);
            root.getChildren().add(gridLine);

            Text text = new Text(i - 15,taskH + 12,Instant.ofEpochSecond((long)(timePosition - (timePosition % 60*interval)*taskW/60) * 60 * 24 - iterator*86400*interval).toString().substring(5,10));
            text.setTextAlignment(TextAlignment.CENTER);
            root.getChildren().add(text);

            iterator++;
        }

        iterator = 0;

        //thick lines
        for(double i = timePosition + ((60-timePosition % 60))*taskW/60 + scrollX + (interval-1) * taskW; i <= canvasWidth; i += taskW*interval){
            Line gridLine = new Line(i,taskH*1.5,i,canvasHeight);
            gridLine.setStrokeWidth(3);
            gridLine.setStroke(Color.GRAY);
            root.getChildren().add(gridLine);

            Text text = new Text(i - 15,taskH + 12,Instant.ofEpochSecond((long)(timePosition + ((60*interval-timePosition % 60*interval))*taskW/60) * 60 * 24 + (interval-1)*86400 + (iterator)*86400*interval).toString().substring(5,10));
            text.setTextAlignment(TextAlignment.CENTER);
            root.getChildren().add(text);

            iterator++;
        }

        //horizontal lines
        for(int i = 1; i < canvasHeight / taskH; i += 1){
            Line gridLine = new Line(0,taskH*i+taskH,canvasWidth,taskH*i+taskH);
            gridLine.setStrokeWidth(3);
            gridLine.setStroke(Color.GRAY);
            root.getChildren().add(gridLine);
        }
    }

    //Oskar
    public void update(){
        root.getChildren().clear();

        double timePosition = (getCurrentTime() - UNIX_START)/60/24;

        drawGrid(root,timePosition);

        if(model.getSelectedProject() != null)
        {
            for(int i = 0; i < model.getSelectedProject().getTasks().size(); i += 1){
                Task task = model.getSelectedProject().getTasks().get(i);
                task.y -= (task.y%taskH);

                int offset = 0;

                if(taskW == 30)
                {
                    offset = -24;
                }
                else if (taskW == 15)
                {
                    offset=-6;
                }

                if (taskResize != "left"){
                    task.x -= task.x % taskW + offset;
                }

                if (task.width < taskW){
                    task.width = taskW;
                }

                drawTask(task.x,task.y,task.width, task.getName(), task.isFinished());
            }
        }
        else
        {
            for(int i = 0; i < model.getProjects().size(); i += 1){
                Project project = model.getProjects().get(i);
                project.y -= (project.y%taskH);

                int offset = 0;

                if(taskW == 30)
                {
                    offset = -24;
                }
                else if (taskW == 15)
                {
                    offset=-6;
                }

                if (taskResize != "left"){
                    project.x -= project.x % taskW + offset;
                }

                if (project.width < taskW){
                    project.width = taskW;
                }

                drawTask(project.x,project.y,project.width, project.getName(), project.isFinished());
            }
        }

        Text currentDate = new Text(0, 20, Instant.ofEpochSecond(getCurrentTime()).toString().substring(0,19));
        currentDate.setFill(Color.BLACK);
        currentDate.setTextAlignment(TextAlignment.CENTER);
        currentDate.setScaleX(1.5);
        currentDate.setScaleY(1.5);
        currentDate.setX(canvasWidth - currentDate.getLayoutBounds().getWidth()*1.25);

        Line currentTimeLine = new Line((getCurrentTime() - UNIX_START)/60/24 + scrollX,taskH*1.5,(getCurrentTime() - UNIX_START)/60/24 + scrollX,canvasHeight);
        currentTimeLine.setStrokeWidth(5);
        currentTimeLine.setStroke(Color.DARKRED);
        
        if(model.getSelectedProject() == null)
        {
            root.getChildren().addAll(currentDate,currentTimeLine);
        }
        else
        {
            if(projectName != null)
                model.getSelectedProject().setName(projectName.getText());
            
            projectName = new TextField();
            projectName.setLayoutX(30);
            projectName.setLayoutY(10);
            projectName.setText(model.getSelectedProject().getName());
            projectName.setScaleX(1.25);
            projectName.setScaleY(1.25);

            if(projectFinished != null)
                model.getSelectedProject().setFinished(projectFinished.isSelected());

            projectFinished = new CheckBox("Finished");
            projectFinished.setSelected(model.getSelectedProject().isFinished());
            projectFinished.setLayoutX(210);
            projectFinished.setLayoutY(13);
            projectFinished.setScaleX(1.25);
            projectFinished.setScaleY(1.25);
            
            root.getChildren().addAll(projectName,projectFinished,currentDate,currentTimeLine);
        }
    }

    //Olav
    public static int getIntegerFromText(TextArea text){
        String number = text.getText();
        return Integer.parseInt(number.replaceAll("[\\D]", ""));
    }

    //Oskar
    private String getTaskResizeProperties(int mx, int my){
        if(model.getSelectedProject() != null)
        {
            hoverTask = null;
            
            for(int i = 0; i < model.getSelectedProject().getTasks().size(); i += 1){
                long taskX = model.getSelectedProject().getTasks().get(i).x;
                int taskY = model.getSelectedProject().getTasks().get(i).y;
                double taskW = model.getSelectedProject().getTasks().get(i).width;
                int offset = 10;

                if (mx >= taskX-offset && my >= taskY && mx <= taskX + taskW+offset && my <= taskY + taskH){
                    hoverTask = model.getSelectedProject().getTasks().get(i);
                    hoverX = mx - model.getSelectedProject().getTasks().get(i).x;
                    hoverY = my - model.getSelectedProject().getTasks().get(i).y;

                    taskResize = "";

                    if (mx <= taskX+offset || mx >= taskX + taskW-offset){
                        if (mx <= taskX+offset){
                            taskResize = "left";
                        } else{
                            taskResize = "right";
                        }
                    }

                    return taskResize;
                }
            }
            return "empty";
        }
        else
        {
            hoverProject = null;
            
            for(int i = 0; i < model.getProjects().size(); i += 1){
                long taskX = model.getProjects().get(i).x;
                int taskY = model.getProjects().get(i).y;
                double taskW = model.getProjects().get(i).width;
                int offset = 10;

                if (mx >= taskX-offset && my >= taskY && mx <= taskX + taskW+offset && my <= taskY + taskH){
                    hoverProject = model.getProjects().get(i);
                    hoverX = mx - model.getProjects().get(i).x;
                    hoverY = my - model.getProjects().get(i).y;

                    taskResize = "";

                    if (mx <= taskX+offset || mx >= taskX + taskW-offset){
                        if (mx <= taskX+offset){
                            taskResize = "left";
                        } else{
                            taskResize = "right";
                        }
                    }

                    return taskResize;
                }
            }
            return "empty";
        }
    }

    //Oskar
    public void drawTask(long x, int y, double width, String name, boolean isFinished){
        Rectangle rect = new Rectangle(x+scrollX,y,width,taskH);
        if (!isFinished && x + width < (getCurrentTime() - UNIX_START)/60/24){
            rect.setFill(Color.rgb(160,50,70));
        } else if (!isFinished){
            rect.setFill(Color.rgb(50,110,160));
        } else {
            rect.setFill(Color.rgb(50,160,70));
        }
        double textSize = taskH / 2;
        Text text = new Text();

        text.setFont(Font.font("verdana", FontWeight.BOLD, FontPosture.REGULAR, textSize));
        text.setText(name);

        if (textSize > taskH / 2){
            textSize = taskH / 2;
        }

        if (text.getLayoutBounds().getWidth() > width){
            textSize = textSize * width / text.getLayoutBounds().getWidth();
        }

        text.setFont(Font.font("verdana", FontWeight.BOLD, FontPosture.REGULAR, textSize));

        text.setX(x+scrollX);
        text.setY(y+text.getLayoutBounds().getHeight());
        text.setFill(Color.rgb(255,255,255));
        root.getChildren().addAll(rect, text);
    }

    @Override
    public void start(Stage stage) {
        //Oskar
        this.model = new Model();
        model.logIn(new User("Test User",1));
        model.setSelectedProject(null);

        Scene scene = new Scene(root, canvasWidth, canvasHeight);
        stage.setTitle("TimeManager");
        stage.setResizable(false);
        stage.setScene(scene);
        stage.show();

        update();

        //Oskar
        scene.setOnMousePressed((MouseEvent event) -> {

            int mx = (int) (event.getX() - scrollX);
            int my = (int) event.getY();

            if (event.getButton() == MouseButton.SECONDARY && getTaskResizeProperties(mx,my) == "empty"){
                int taskX = mx;
                int taskY = my;
                
                if(model.getSelectedProject() == null)
                {
                    model.createProject("Test Project", 2, 4);

                    model.getProjects().get(model.getProjects().size() - 1).x = taskX;
                    model.getProjects().get(model.getProjects().size() - 1).y = taskY;
                    model.getProjects().get(model.getProjects().size() - 1).width = taskW;
                }
                else
                {
                    model.getSelectedProject().getTasks().add( new Task("Test Task",2,4,"This is a test",model.getUserCount()) );

                    model.getSelectedProject().getTasks().get(model.getSelectedProject().getTasks().size()-1).x = taskX;
                    model.getSelectedProject().getTasks().get(model.getSelectedProject().getTasks().size()-1).y = taskY;
                    model.getSelectedProject().getTasks().get(model.getSelectedProject().getTasks().size()-1).width = taskW;
                }

                update();
            } else{
                exitEditMenu(mx+scrollX,my);

                getTaskResizeProperties(mx,my);

                if (hoverTask != null){
                    hoverTaskX = hoverTask.x;
                    hoverTaskY = hoverTask.y;
                    hoverTaskW = (int)hoverTask.width;
                    mouseTaskX = (int)event.getX();
                    mouseTaskY = (int)event.getY();
                    mouseTaskW = (int)hoverTask.width;
                }
                else if (hoverProject != null){
                    hoverTaskX = hoverProject.x;
                    hoverTaskY = hoverProject.y;
                    hoverTaskW = (int)hoverProject.width;
                    mouseTaskX = (int)event.getX();
                    mouseTaskY = (int)event.getY();
                    mouseTaskW = (int)hoverProject.width;
                }
            }
        });

        //Oskar
        scene.setOnMouseReleased((MouseEvent event) -> {
            long mx = (long)event.getX() - scrollX;
            int my = (int) event.getY();

            boolean canSelect = false;

            if (hoverTask != null){
                hoverTask.width = (Math.ceil( hoverTask.width / taskW)) * taskW;
                taskResize = "";

                if (!(mx == mouseTaskX - scrollX && my == mouseTaskY)){
                    selectedTask = null;
                } else{
                    selectedTask = hoverTask;
                }
            }
            else if (hoverProject != null){
                hoverProject.width = (Math.ceil( hoverProject.width / taskW)) * taskW;
                taskResize = "";

                if (!(mx == mouseTaskX - scrollX && my == mouseTaskY)){
                    canSelect = false;
                } else{
                    canSelect = true;
                }
            }

            if (!editMenu){
                update();
            }

            if (hoverTask != null && selectedTask != null && selectedTask == hoverTask && !editMenu){
                drawEditUI();
                editMenu = true;
            }

            if (hoverProject != null && !editMenu && canSelect){
                model.setSelectedProject(hoverProject);
                update();
            }
        });

        //Oskar
        scene.setOnMouseDragged((MouseEvent event) -> {
            long mx = (long)event.getX() - scrollX;
            int my = (int) event.getY();

            if (hoverTask != null && !editMenu){
                if (taskResize == ""){
                    hoverTask.x = mx - hoverX;
                    hoverTask.y = my - hoverY;
                } else if (taskResize == "right"){
                    hoverTask.width = mx - hoverTask.x;
                } else if (taskResize == "left"){
                    hoverTask.x = mx - hoverX;
                    hoverTask.width = hoverTaskW+(hoverTaskX-hoverTask.x);
                }

                update();
            }
            else if (hoverProject != null && !editMenu){
                if (taskResize == ""){
                    hoverProject.x = mx - hoverX;
                    hoverProject.y = my - hoverY;
                } else if (taskResize == "right"){
                    hoverProject.width = mx - hoverProject.x;
                } else if (taskResize == "left"){
                    hoverProject.x = mx - hoverX;
                    hoverProject.width = hoverTaskW+(hoverTaskX-hoverProject.x);
                }

                update();
            }
        });

        //oskar
        scene.setOnScroll((ScrollEvent event) -> {
            if (controlGetKey){
                if (event.getDeltaY() > 0 && taskW != 15){
                    int prevW = taskW;
                    taskW -= 15 + ((taskW == 60) ? 15 : 0);

                    if(model.getSelectedProject() != null)
                    {
                        for(int i = 0; i < model.getSelectedProject().getTasks().size(); i += 1){
                            model.getSelectedProject().getTasks().get(i).width = (model.getSelectedProject().getTasks().get(i).width / prevW) * taskW;

                            double timePosition = (getCurrentTime() - UNIX_START)/60/24;                        
                            double distance = (timePosition - (timePosition % 60)*prevW/60) - model.getSelectedProject().getTasks().get(i).x;

                            distance /= prevW;

                            if(taskW == 30)
                                model.getSelectedProject().getTasks().get(i).x += distance * taskW;
                            else
                                model.getSelectedProject().getTasks().get(i).x += (distance+1) * taskW;
                        }
                    }
                    
                    update();
                } else if (event.getDeltaY() < 0 && taskW != 60){
                    int prevW = taskW;
                    taskW += 15 + ((taskW == 30) ? 15 : 0);

                    if(model.getSelectedProject() != null)
                    {
                        for(int i = 0; i < model.getSelectedProject().getTasks().size(); i += 1){
                            model.getSelectedProject().getTasks().get(i).width = (model.getSelectedProject().getTasks().get(i).width / prevW) * taskW;
                            double timePosition = (getCurrentTime() - UNIX_START)/60/24;                        
                            double distance = (timePosition - (timePosition % 60)*prevW/60) - model.getSelectedProject().getTasks().get(i).x;
                            
                            distance /= prevW;

                            if(taskW == 60)
                                model.getSelectedProject().getTasks().get(i).x -= Math.floor(distance) * prevW;
                            else
                                model.getSelectedProject().getTasks().get(i).x -= (distance+1) * prevW;
                        }
                    }
                    
                    update();
                }
            } else{
                if (event.getDeltaY() > 0){
                    scrollX += 32;
                    update();
                } else if (event.getDeltaY() < 0){
                    scrollX -= 32;
                    update();
                }
            }
        });

        //Oskar
        scene.setOnKeyPressed((KeyEvent event) ->{
            if (event.getCode() == KeyCode.CONTROL) {
                controlGetKey = true;
            }
            else if (model.getSelectedProject() != null && event.getCode() == KeyCode.ESCAPE) {
                update();
                model.setSelectedProject(null);
                projectName = null;
                projectFinished = null;
                exitEditMenu(0, 0);
                selectedTask = null;
                hoverTask = null;
                editMenu = false;
                update();
            }
        });

        //Oskar
        scene.setOnKeyReleased((KeyEvent event) ->{
            if (event.getCode() == KeyCode.CONTROL) {
                controlGetKey = false;
            }
        });
    }

        public static ArrayList<Task> arrangeTasks(Project project) { //sigurd
            ArrayList<Task> tasks = project.getTasks();
            long[] startDates = new long[tasks.size()];
            for(int i = 0; i < tasks.size(); i++) {
                startDates[i] = (tasks.get(i).getStartDate());
            }
            for(int i = 0; i < startDates.length-1; i++){
                for(int j = 0; j < startDates.length-i-1; j++){ //bubblesort
                    if(startDates[j] > startDates[j+1]){
                        long temp = startDates[j];
                        startDates[j] = startDates[j+1];
                        startDates[j+1] = temp;
                        Task temptask = tasks.get(j);
                        tasks.set(j,tasks.get(j+1));
                        tasks.set(j+1,temptask);
                    }
                }
            } //tasks sorteret efter startDate
            return tasks;
        }
    
        public static int[] calculateYlevel(ArrayList<Task> tasks){ //olav
            int[] yLevel = new int[tasks.size()];
            java.util.Arrays.fill(yLevel, 0);
            for(int i = 0; i<yLevel.length; i++) {
                long currentStartDate = tasks.get(i).getStartDate();
                long currentEndDate = tasks.get(i).getEndDate();
                boolean conflicts=true;
                while (conflicts){
                    conflicts=false;
                    for (int j = 0; j<yLevel.length; j++){
                        long compareStartDate = tasks.get(j).getStartDate();
                        long compareEndDate = tasks.get(j).getEndDate();
                        if(j!=i){
                            if (yLevel[i]==yLevel[j]){
                                if(isOverlapping(currentStartDate, currentEndDate, compareStartDate, compareEndDate)){
                                    yLevel[i]++;
                                    conflicts=true;
                                }
                            }
                        }
                    }
                }
            }
        return yLevel;
        }
        public static boolean isOverlapping(long startDate, long endDate, long startDate2, long endDate2) { //sigurd
            if((startDate < endDate2 && startDate > startDate2) || (endDate > startDate2 && endDate < endDate2) ){
                return true;
            }
            return false;
    
        }

    //Martin
    private long getCurrentTime()
    {
        return Instant.now().getEpochSecond() + 7200;
    }

    public static void main(String[] args) {
        launch();
    }

}