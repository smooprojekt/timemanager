package application;

import java.util.ArrayList;

public class Project {
    private String name;
    @SuppressWarnings("unused")
    private long unixStartDate;
    @SuppressWarnings("unused")
    private long unixEndDate;
    private boolean finished;

    private ArrayList<Task> tasks = new ArrayList<Task>();

    public long x;
    public int y;
    public double width;

    //Martin
    public Project(String name, long startDate, long endDate)
    {
        this.name = name;
        this.unixStartDate = startDate;
        this.unixEndDate = endDate;
        this.finished = false;
    }

    //Martin
    public void createTask(String taskName, long taskStartDate, long taskEndDate, String taskDescription, int userCount)
    {
        tasks.add(new Task(taskName,taskStartDate,taskEndDate, taskDescription, userCount));
    }

    //Martin
    public void finishTask(Task task)
    {
        task.setFinished(true);
    }

    //Martin
    public boolean isFinished()
    {
        return finished;
    }

    //Martin
    public void setFinished(boolean finished)
    {
        this.finished = finished;
    }

    //Martin
    public String getName()
    {
        return this.name;
    }

    //Martin
    public void setName(String name)
    {
        this.name = name;
    }

    //Martin
    public ArrayList<Task> getTasks()
    {
        return tasks;
    }

    //Martin
    public boolean hasTaskWithName(String name)
    {
        for(int i = 0; i < tasks.size(); i++)
        {
            if(tasks.get(i).getName().equals(name))
            {
                return true;
            }
        }

        return false;
    }

    //Martin
    public Task taskFromName(String name)
    {
        for(int i = 0; i < tasks.size(); i++)
        {
            if(tasks.get(i).getName().equals(name))
            {
                return tasks.get(i);
            }
        }

        return null;
    }
}
