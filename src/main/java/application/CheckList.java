package application;

import java.util.ArrayList;

public class CheckList {
    //Oskar
    private ArrayList<String> checkListText = new ArrayList<>();
    private ArrayList<Boolean> checkListChecked = new ArrayList<>();

    public CheckList(){

    }

    //Oskar
    public String getCheckListName(int index){
        return checkListText.get(index);
    }

    //Oskar
    public boolean getCheckListChecked(int index){
        return checkListChecked.get(index);
    }
    
    //Oskar
    public int getListSize(){
        return checkListText.size();
    }

    //Oskar
    public void addListText(String name){
        checkListText.add(name);
    }

    ////Oskar
    public void addListChecked(boolean check){
        checkListChecked.add(check);
    }

    //Oskar
    public void setListText(int index, String name){
        checkListText.set(index, name);
    }

    //Oskar
    public void setListChecked(int index, boolean check){
        checkListChecked.set(index, check);
    }
}
