package example.cucumber;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Given;

import application.*;

public class StepDefinitions {

	private User user;
	private Model model;
	private Task task;

	public StepDefinitions()
	{
		this.user = new User("name",1);
		this.model = new Model();
	}

	@Given("the user is logged in")
	public void theUserIsLoggedIn() {
		model.logIn(user);
	}

	@When("the user creates a project with name {string}")
	public void theUserCreatesAProjectWithName(String string) {
		model.createProject(string, 0, 0);
		model.setSelectedProject(model.projectFromName(string));
	}

	@Then("the project with name {string} is in the project list")
	public void theProjectWithNameIsInTheProjectList(String string) {
		model.hasProjectWithName(string);
	}
	
	@Given("the user is an Employee")
	public void theUserIsAnEmployee() {
		user.setClearance(0);
	}

	@When("the user is Employee")
	public void theUserIsEmployee() {
		assertTrue(user.getClearance() == 0);
	}

	@Then("the user is Project Leader")
	public void theUserIsProjectLeader() {
		assertTrue(model.checkClearance(user) == 1);
	}

	@Then("the project with name {string} is not in the project list")
	public void theProjectWithNameIsNotInTheProjectList(String string) {
		assertFalse(model.hasProjectWithName(string));
	}


	@Given("the user is a Project Leader")
	public void theUserIsAProjectLeader() {
		user.setClearance(1);
	}

	@Given("a project with name {string} exists")
	public void aProjectWithNameExists(String string) {
		user.setClearance(1);
		model.createProject(string, 0, 0);
	}

	@When("the user creates a task with name {string} under {string}")
	public void theUserCreatesATaskWithNameUnder(String string, String string2) {
		model.createTask(model.projectFromName(string2),string,0,0,"description",model.getUserCount());
	}

	@Then("the task with name {string} is in the project with name {string}'s task list")
	public void theTaskWithNameIsInTheProjectWithNameSTaskList(String string, String string2) {
		assertTrue(model.projectFromName(string2).hasTaskWithName(string));
	}

    @When("the user opens the project called {string}")
    public void the_user_opens_the_project(String name) {
        assertTrue(model.getSelectedProject() == model.projectFromName(name));
    }

    @Given("the project with name {string} contains multiple tasks")
    public void the_project_with_name_contains_multiple_tasks(String string) {
		model.projectFromName(string).createTask("task1", 0, 0, "description", 0);
		model.projectFromName(string).createTask("task2", 0, 0, "description", 0);
        assertTrue(model.projectFromName(string).getTasks().size() > 1);
    }

    @Then("the tasks are sorted and displayed")
    public void the_tasks_are_sorted_and_displayed() {
        assertTrue(model.getSelectedProject().getTasks() == model.getSelectedProject().getTasks());
	}

	@When("the user adds a checklist item with name {string} to a task")
	public void theUserAddsAChecklistItemToATask(String string) {
		task = new Task("testTask",0,0,"description",1);
		task.checkList.addListChecked(false);
        task.checkList.addListText(string);
	}

	@Then("the task has a checklist item with name {string} in position 0")
	public void theTaskHasTheChecklistAttribute(String string) {
		assertTrue(task.checkList.getCheckListName(0).equals(string));
	}

	@Given("an unchecked checklist item with name {string} exists on a task")
	public void anUncheckedChecklistItemWithNameExistsOnATask(String string) {
		task = new Task("testTask",0,0,"description",1);
		task.checkList.addListChecked(false);
        task.checkList.addListText(string);
	}

	@When("the user checks the checklist item with name {string}")
	public void theUserChecksTheChecklistItemWithName(String string) {
		task.checkList.setListChecked(0, true);
	}

	@Then("the checklist item with name {string} is checked")
	public void theChecklistItemWithNameIsChecked(String string) {
		assertTrue(task.checkList.getCheckListChecked(0));
	}

	@When("the user changes the name of the checklist item with name {string} to {string}")
	public void theUserChangesTheNameOfTheChecklistItemWithNameTo(String string, String string2) {
		task.checkList.setListText(0, string2);
	}

	@Then("the checklist item list has size {int}")
	public void theChecklistItemListHasSize(Integer int1) {
		assertTrue(task.checkList.getListSize() == 1);
	}

	@When("changes the name of task with name {string} to {string} under project with name {string}")
	public void changesTheNameOfTaskWithNameTo(String string, String string2, String string3) {
		model.projectFromName(string3).taskFromName(string).setName(string2);
	}

	@When("the user becomes an Employee")
	public void theUserBecomesAnEmployee() {
		user.setClearance(0);
	}

	@When("the user becomes a Project Leader")
	public void theUserBecomesAProjectLeader() {
		user.setClearance(1);
	}

	@Given("the user has an ID")
	public void theUserHasAnID() {
		assertTrue(user.getID()!=-1);
	}

	@When ("the user gets an ID with the number {int}")
	public void theUserGetsAnIDWithTheNumber(Integer int1) {
		user.setID(int1);
	}

	@Then ("the user has an ID with the number {int}")
	public void theUserHasAnIDWithTheNumber(Integer int1) {
		assertTrue(user.getID()==int1);
	}

	@When("the user adds {int} hours to the task with name {string} under project with name {string}")
	public void theUserAddsHoursToTheTaskWithName(Integer int1, String string, String string2) {
		user.assignTime(model.projectFromName(string2).taskFromName(string),int1);
	}

	@Then("the user has logged a total of {int} hours on the project with name {string}")
	public void theUserHasLoggedATotalOfHoursOnTheTaskWithNameTestTask(Integer int1, String string) {
		assertTrue(user.getTimeTotal(model.projectFromName(string)) == int1);
	}

	@Then("the task with name {string} under project with name {string} has a total of {int} hours logged in it")
	public void theTaskWithNameUnderProjectWithNameHasATotalOfHoursLoggedInIt(String string, String string2, Integer int1) {
		assertTrue(model.projectFromName(string2).taskFromName(string).getTotalTime() == int1);
	}

	@When("the user sets the task with name {string} under {string} as finished")
	public void theUserSetsTheTaskWithNameUnderAsFinished(String string, String string2) {
		model.finishTask(model.projectFromName(string2),model.projectFromName(string2).taskFromName(string));
	}

	@Then("the task with name {string} under {string} is finished")
	public void theTaskWithNameUnderIsFinished(String string, String string2) {
		assertTrue(model.projectFromName(string2).taskFromName(string).isFinished());
	}

	@When("the user sets the project with name {string} as finished")
	public void theUserSetsTheProjectWithNameAsFinished(String string) {
		model.finishProject(model.projectFromName(string));
	}

	@Then("the project with name {string} is finished")
	public void theProjectWithNameIsFinished(String string) {
		assertTrue(model.projectFromName(string).isFinished());
	}

	@Then("the currently logged in user is the user")
	public void theCurrentlyLoggedInUserIsTheUser() {
		assertTrue(model.getUser() == user && model.loggedIn);
	}
}