## Name
TimeManager

## Description
Software til tid- og projektstyring til virksomheder.

## Authors and acknowledgment
Udviklere: Sigurd Fajstrup Jørgensen, Martin Handest, Oskar William Ulrich Holland, Olav Kristian Dehn
## License
For open source projects, say how it is licensed.

## Project status
Færdig-agtig
