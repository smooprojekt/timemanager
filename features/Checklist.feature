#Martin
Feature: Checklist
    Description: Checklist tests
    Actors: User

Scenario: User adds checklist item to task
    Given the user is logged in
    And the user is an Employee
    When the user adds a checklist item with name "MyCheck" to a task
    Then the task has a checklist item with name "MyCheck" in position 0
    And the checklist item list has size 1

Scenario: User checks an item on a checklist
    Given the user is logged in
    And the user is an Employee
    And an unchecked checklist item with name "MyCheck" exists on a task
    When the user checks the checklist item with name "MyCheck"
    Then the checklist item with name "MyCheck" is checked

Scenario: User changes name of checklist item
    Given the user is logged in
    And the user is an Employee
    And an unchecked checklist item with name "MyCheck" exists on a task
    When the user changes the name of the checklist item with name "MyCheck" to "MyOtherCheck"
    Then the task has a checklist item with name "MyOtherCheck" in position 0