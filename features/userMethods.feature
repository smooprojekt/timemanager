#Olav
Feature: The user changes characteristics
    Description: The user changes characteristics
    Actors: User

Scenario: The user loses clearance
    Given the user is Project Leader
    When the user becomes an Employee
    Then the user is an Employee

Scenario: The user gains clearance
    Given the user is an Employee
    When the user becomes a Project Leader
    Then the user is Project Leader

Scenario: The user gets an ID
    Given the user has an ID
    When the user gets an ID with the number 1234
    Then the user has an ID with the number 1234

Scenario: The user adds hours to a task
    Given the user is logged in
    And a project with name "test project" exists
    When the user creates a task with name "test task" under "test project"
    And the user adds 2 hours to the task with name "test task" under project with name "test project"
    Then the user has logged a total of 2 hours on the project with name "test project"
    And the task with name "test task" under project with name "test project" has a total of 2 hours logged in it

Scenario: The user logs in
    Given the user is logged in
    Then the currently logged in user is the user