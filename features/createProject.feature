#Martin
Feature: Create project
    Description: A user creates a project
    Actors: User

Scenario: User creates project successfully
    Given the user is logged in
    And the user is a Project Leader
    When the user is Project Leader
    And the user creates a project with name "test project"
    Then the project with name "test project" is in the project list

Scenario: User fails to create project
    Given the user is logged in
    And the user is an Employee
    When the user is Employee
    And the user creates a project with name "test project"
    Then the project with name "test project" is not in the project list