#Martin
Feature: Create task
    Description: A user creates a task
    Actors: User

Scenario: User creates task
    Given the user is logged in
    And a project with name "test project" exists
    And the user is a Project Leader
    When the user is Project Leader
    And the user creates a task with name "test task" under "test project"
    Then the task with name "test task" is in the project with name "test project"'s task list