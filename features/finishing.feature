#Martin
Feature: Finishing
    Description: Finishing tasks and projects
    Actors: User

Scenario: Finishing a task
    Given the user is logged in
    And a project with name "test project" exists
    And the user creates a task with name "test task" under "test project"
    When the user sets the task with name "test task" under "test project" as finished
    Then the task with name "test task" under "test project" is finished

Scenario: Finishing a project
    Given the user is logged in
    And a project with name "test project" exists
    And the user is a Project Leader
    When the user is Project Leader
    And the user sets the project with name "test project" as finished
    Then the project with name "test project" is finished