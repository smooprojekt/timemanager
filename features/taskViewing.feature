#Olav
Feature: View tasks in the current project
    Description: A user views available tasks
    Actors: User

Scenario: A user views the project
    Given the user is logged in
    When the user creates a project with name "test project"
    And the project with name "test project" contains multiple tasks
    When the user opens the project called "test project"
    Then the tasks are sorted and displayed

Scenario: A user creates a new task
    Given the user is logged in
    When the user creates a project with name "test project"
    And the user is a Project Leader
    And the project with name "test project" contains multiple tasks
    When the user creates a task with name "test task" under "test project"
    Then the tasks are sorted and displayed

Scenario: A user changes the name of a task
    Given the user is logged in
    And a project with name "test project" exists
    And the user is an Employee
    When the user creates a task with name "test task" under "test project"
    And changes the name of task with name "test task" to "other test task" under project with name "test project"
    Then the task with name "other test task" is in the project with name "test project"'s task list
